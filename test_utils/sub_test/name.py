def name(name=None):
    if name is None:
        return "How are you doing Random Person?"
    else:
        return f"How are you doing {name}?"
