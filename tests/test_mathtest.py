from test_utils.mathtest import add
from test_utils.mathtest import multiply

def test_add_postives():
    assert add(1,1) == 2

def test_add_negatives():
    assert add(-1,-1) == -2

def test_add_postive_negative():
    assert add(2,-1) == 1

def test_add_decimals():
    assert add(1.5,1.7) == 3.2

def test_multiply_postives():
    assert multiply(3,4) == 12

def test_multiply_negatives():
    assert multiply(-2,-4) == 8

def test_multiply_positive_negative():
    assert multiply(2,-3) == -6
