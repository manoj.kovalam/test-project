from test_utils.helloworld import say_hello

def test_helloworld_no_params():
    assert say_hello() == "Hello World!"

def test_helloworld_params():
    assert say_hello("Manoj") == "Hello Manoj!"
