from test_utils_2.mathtest_2 import subtract
from test_utils_2.mathtest_2 import divide

def test_divide_postives():
    assert divide(4,1) == 4

def test_divide_negatives():
    assert divide(-8,-2) == 4

def test_divide_postive_negative():
    assert divide(2,-1) == -2

def test_subtract_postives():
    assert subtract(3,4) == -1

def test_subtract_negatives():
    assert subtract(-2,-4) == 2

def test_subtract_positive_negative():
    assert subtract(2,-3) == 5
