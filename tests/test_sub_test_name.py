from test_utils.sub_test.name import name

def test_name_no_params():
    assert name() == "How are you doing Random Person?"

def test_name_params():
    assert name("Manoj") == "How are you doing Manoj?"
