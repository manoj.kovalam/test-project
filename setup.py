import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="test_utils",
    version="0.5",
    author="Manoj Kovalam",
    author_email="manoj.kovalam@ligo.org",
    description="A test package",
    long_description=long_description,
    long_description_content_type="text/markdown",
    #insert URL
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3.8",
        "License :: OSI Approved :: GNU License",
        "Operating System :: UNIX",
    ],
    extras_require = {
        'dev':  ['pytest>=3.7',],
    },
)
